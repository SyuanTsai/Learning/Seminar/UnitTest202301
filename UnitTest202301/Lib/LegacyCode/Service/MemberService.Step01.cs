﻿using Lib.LegacyCode.Context;

namespace Lib.LegacyCode.Service;

public class MemberServiceStep01
{
    /// <summary>
    ///     模擬從DB取回雜湊後的密碼
    /// </summary>
    /// <param name="account"></param>
    /// <returns></returns>
    public string GetDbMd5Hash(string account)
    {
        return DbContext.GetPassword(account);
    }
}
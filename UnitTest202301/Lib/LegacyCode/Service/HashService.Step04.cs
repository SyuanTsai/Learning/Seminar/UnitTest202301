﻿using System.Text;
using Lib.LegacyCode.Interface;

namespace Lib.LegacyCode.Service;

public class HashServiceStep04 : IHashServiceStep04
{
    /// <summary>
    ///     雜湊
    /// </summary>
    /// <param name="memberPassword"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public string GetDbMd5Hash(string memberPassword)
    {
        //將字串編碼成 UTF8 位元組陣列
        var bytes = Encoding.UTF8.GetBytes(memberPassword);

        //取得雜湊值位元組陣列
        var hash = System.Security.Cryptography.MD5.HashData(bytes);

        //取得 MD5
        var md5 = BitConverter.ToString(hash)
            .Replace("-", String.Empty)
            .ToUpper();

        return md5;
    }
}
﻿using Lib.LegacyCode.Context;
using Lib.LegacyCode.Interface;

namespace Lib.LegacyCode.Service;

public class MemberServiceStep04 : IMemberServiceStep04
{
    /// <summary>
    ///     模擬從DB取回雜湊後的密碼
    /// </summary>
    /// <param name="account"></param>
    /// <returns></returns>
    public string GetMd5Hash(string account)
    {
        return DbContext.GetPassword(account);
    }
}
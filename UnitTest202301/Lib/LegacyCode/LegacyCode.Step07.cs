﻿using Lib.LegacyCode.Interface;
using Lib.LegacyCode.Service;

namespace Lib.LegacyCode;

public class LegacyCodeStep07
{
    // 修改static變成readonly
    private readonly IHashServiceStep04 _hash;
    private readonly IMemberServiceStep04 _member;

    // 改用DI的方式提供注入點(接縫)，讓外部決定要注入哪個實作，
    // 透過這邊讓測試可以進行mock
    public LegacyCodeStep07(IHashServiceStep04 hash, IMemberServiceStep04 member)
    {
        _hash = hash;
        _member = member;
    }

    // 此時原本的程式依舊使用原本的constructor。
    public LegacyCodeStep07()
    {
        _hash = new HashServiceStep04();
        _member = new MemberServiceStep04();
    }


    /// <summary>
    ///     驗證使用者的帳號密碼雜湊後的密碼是否正確
    /// </summary>
    /// <param name="account"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public bool IsValid(string account, string password)
    {
        var memberPassword = GetPassWord(account);

        var hashResult = GenerateHash(account, password);

        var isValid = memberPassword == hashResult;
        if (isValid)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  移除static
    private string GenerateHash(string account, string password)
    {
        // _hash = new HashServiceStep04();
        var validHash = $"{account}{password}";
        var hashResult = _hash.GetDbMd5Hash(validHash);
        return hashResult;
    }

    //  移除static
    private string GetPassWord(string account)
    {
        // _member = new MemberServiceStep04();
        string memberPassword = _member.GetMd5Hash(account);
        return memberPassword;
    }
}
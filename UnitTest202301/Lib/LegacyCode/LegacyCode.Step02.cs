﻿using Lib.LegacyCode.Service;

namespace Lib.LegacyCode;

public class LegacyCodeStep02
{
    /// <summary>
    ///     驗證使用者的帳號密碼雜湊後的密碼是否正確
    /// </summary>
    /// <param name="account"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public bool IsValid(string account, string password)
    {
        //  抽取方法
        var memberPassword = GetPassWord(account);
        
        //  抽取方法
        var hashResult = GenerateHash(account, password);

        var isValid = memberPassword == hashResult;
        if (isValid)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static string GenerateHash(string account, string password)
    {
        var hash = new HashServiceStep01();
        var validHash = $"{account}{password}";
        var hashResult = hash.GetDbMd5Hash(validHash);
        return hashResult;
    }

    private static string GetPassWord(string account)
    {
        var member = new MemberServiceStep01();
        string memberPassword = member.GetDbMd5Hash(account);
        return memberPassword;
    }
}
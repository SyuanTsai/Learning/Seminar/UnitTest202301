﻿namespace Lib.LegacyCode.Context;

public class DbContext
{
    
    private static readonly Dictionary<string, string> Profiles;

    static DbContext()
    {
        Profiles = new Dictionary<string, string>();
        Profiles.Add("Syuan", "Tsai");
    }

    public static string GetPassword(string key)
    {
        return Profiles[key];
    }
}
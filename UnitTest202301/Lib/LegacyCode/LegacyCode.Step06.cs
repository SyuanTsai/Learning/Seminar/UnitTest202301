﻿using Lib.LegacyCode.Interface;
using Lib.LegacyCode.Service;

namespace Lib.LegacyCode;

public class LegacyCodeStep06
{
    private static IHashServiceStep04 _hash;
    private static IMemberServiceStep04 _member;

    // 轉移到Constructor，此時程式邏輯一樣沒變。
    public LegacyCodeStep06()
    {
        _hash = new HashServiceStep04();
        _member = new MemberServiceStep04();
    }


    /// <summary>
    ///     驗證使用者的帳號密碼雜湊後的密碼是否正確
    /// </summary>
    /// <param name="account"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public bool IsValid(string account, string password)
    {
        var memberPassword = GetPassWord(account);

        var hashResult = GenerateHash(account, password);

        var isValid = memberPassword == hashResult;
        if (isValid)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static string GenerateHash(string account, string password)
    {
        // 轉移到Constructor
        // _hash = new HashServiceStep04();
        var validHash = $"{account}{password}";
        var hashResult = _hash.GetDbMd5Hash(validHash);
        return hashResult;
    }

    private static string GetPassWord(string account)
    {
        // 轉移到Constructor
        // _member = new MemberServiceStep04();
        string memberPassword = _member.GetMd5Hash(account);
        return memberPassword;
    }
}
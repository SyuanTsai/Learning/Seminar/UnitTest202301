namespace Lib.LegacyCode.Interface;

public interface IHashServiceStep04
{
    /// <summary>
    ///     雜湊
    /// </summary>
    /// <param name="memberPassword"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    string GetDbMd5Hash(string memberPassword);
}
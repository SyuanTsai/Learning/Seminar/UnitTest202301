namespace Lib.LegacyCode.Interface;

public interface IMemberServiceStep04
{
    /// <summary>
    ///     模擬從DB取回雜湊後的密碼
    /// </summary>
    /// <param name="account"></param>
    /// <returns></returns>
    string GetMd5Hash(string account);
}
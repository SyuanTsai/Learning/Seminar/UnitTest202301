﻿namespace Lib.FileCheck;

/// <summary>
///     檔案檢察功能
/// </summary>
public class FileCheckStep01
{
    /// <summary>
    ///     檢查檔案是否存在
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool IsValidFileExists(string fileName)
    {
        //  依賴點
        if (!File.Exists(fileName))
        {
            return false;
        }

        return true;
    }
}
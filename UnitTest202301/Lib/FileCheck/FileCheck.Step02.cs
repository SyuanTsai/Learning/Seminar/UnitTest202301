﻿namespace Lib.FileCheck;

/// <summary>
///     檔案檢察功能
/// </summary>
public class FileCheckStep02
{
    /// <summary>
    ///     檢查檔案是否存在
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool IsValidFileExists(string fileName)
    {
        //  依賴點
        if (FileDependence(fileName))
        {
            return false;
        }

        return true;
    }

    /// <summary>
    ///     分離出依賴點，
    ///     1. 可以透過工具(IDE)來提升速度、
    ///     2. 同時藉由工具完成，不會去異動到原始的邏輯防止問題產生。
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private static bool FileDependence(string fileName)
    {
        return !File.Exists(fileName);
    }
}
﻿namespace Lib.FileCheck;

/// <summary>
///     檔案檢察功能
/// </summary>
public class FileCheckStep03
{
    /// <summary>
    ///     檢查檔案是否存在
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public bool IsValidFileExists(string fileName)
    {
        //  依賴點
        if (FileDependence(fileName))
        {
            return false;
        }

        return true;
    }

    /// <summary>
    ///     從私有改成 protected virtual 讓後續繼承的時候可以改變這個方法的行為。
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    protected virtual bool FileDependence(string fileName)
    {
        return !File.Exists(fileName);
    }
}
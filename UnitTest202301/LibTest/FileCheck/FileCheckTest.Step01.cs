namespace LibTest.FileCheck;

public class FileCheckTestStep01
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void When_FilePath_Exists()
    {
        //  使用假物件(stub)，並指派偽造的值。
        var sutObj = new FakeFileCheckStep05();
        sutObj.FileDependenceSetter = true;

        var actual = sutObj.IsValidFileExists("SomeFilePath");
        
        Assert.IsTrue(actual, "當檔案存在的時候應該回傳true。");
    }
}
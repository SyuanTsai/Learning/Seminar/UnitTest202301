namespace LibTest.FileCheck;

public class FileCheckTestStep02
{
    private FakeFileCheckStep05? _sutObj;

    [SetUp]
    public void Setup()
    {
        _sutObj = new FakeFileCheckStep05();
    }

    [Test]
    public void When_FilePath_Exists()
    {
        GivenFileDependenceResult(true);
        AssertResultExists("SomeFilePath");
    }

    private void AssertResultExists(string fileName)
    {
        var actual = _sutObj!.IsValidFileExists(fileName);
        Assert.IsTrue(actual, "當檔案存在的時候應該回傳true。");
    }

    private void GivenFileDependenceResult(bool actual)
    {
        _sutObj!.FileDependenceSetter = actual;
    }
}
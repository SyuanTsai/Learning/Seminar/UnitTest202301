﻿using Lib.FileCheck;

namespace LibTest.FileCheck;

/// <summary>
///     建立Field並派給相依欄位
/// </summary>
public class FakeFileCheckStep05 : FileCheckStep03
{
    private bool _fileDependence;

    /// <summary>
    ///     由於是測試類別，
    ///     在設計上只使用setter讓值可以被替換，
    ///     但不讓外部可以不正當存取。
    /// </summary>
    public bool FileDependenceSetter
    {
        set => _fileDependence = value;
    }
    
    /// <summary>
    ///     覆寫父類的原始方法
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    protected override bool FileDependence(string fileName)
    {
        // _fileDependence = base.FileDependence(fileName);
        // 檔案是否存在的結果不再從相依的File取得驗證，而是改由測試類別自行設定
        return _fileDependence;
    }
}
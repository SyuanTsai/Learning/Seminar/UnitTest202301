﻿using Lib.LegacyCode;
using Lib.LegacyCode.Interface;

using NSubstitute;

namespace LibTest.LegacyCode;

public class LegacyCodeTestStep10
{
private IHashServiceStep04? _hash;
private IMemberServiceStep04? _member;
private LegacyCodeStep07 _legacyCode;

[SetUp]
public void Setup()
{
    _hash = Substitute.For<IHashServiceStep04>();
    _member = Substitute.For<IMemberServiceStep04>();
    _legacyCode = new LegacyCodeStep07(_hash , _member);
}

[Test]
public void When_PassWord_Valid()
{
    GivenDbPassword("Md5Password");
    GivenMd5Result("Md5Password");
    //  因為輸入值並不是驗證的重點，所以不需要顯示出來。
    AssertIsValid();
}

private void AssertIsValid()
{
    var actual = _legacyCode.IsValid("Syuan", "Password");
    Assert.IsTrue(actual);
}

private void GivenMd5Result(string returnThis)
{
    _member.GetMd5Hash(Arg.Any<string>()).Returns(returnThis);
}

private void GivenDbPassword(string returnThis)
{
    _hash.GetDbMd5Hash(Arg.Any<string>()).Returns(returnThis);
}
}
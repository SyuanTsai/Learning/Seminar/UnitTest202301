﻿using Lib.LegacyCode;
using Lib.LegacyCode.Interface;

using NSubstitute;

namespace LibTest.LegacyCode;

public class LegacyCodeTestStep08
{
    
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void When_PassWord_Valid()
    {
        var hash = Substitute.For<IHashServiceStep04>();
        hash.GetDbMd5Hash(Arg.Any<string>()).Returns("Md5Password");
        var member = Substitute.For<IMemberServiceStep04>();
        member.GetMd5Hash(Arg.Any<string>()).Returns("Md5Password");
        
        var legacyCode = new LegacyCodeStep07(hash , member);

        //  此時的輸入值其實不重要，因為要驗證的邏輯是，當密碼與DB密碼的MD5是一致的時候。
        var actual = legacyCode.IsValid("Syuan", "Password");
        Assert.IsTrue(actual);
    }
}
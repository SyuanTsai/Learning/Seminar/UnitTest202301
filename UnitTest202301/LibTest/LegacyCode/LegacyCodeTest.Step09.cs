﻿using Lib.LegacyCode;
using Lib.LegacyCode.Interface;

using NSubstitute;

namespace LibTest.LegacyCode;

public class LegacyCodeTestStep09
{
private IHashServiceStep04? _hash;
private IMemberServiceStep04? _member;
private LegacyCodeStep07 _legacyCode;

[SetUp]
public void Setup()
{
    _hash = Substitute.For<IHashServiceStep04>();
    _member = Substitute.For<IMemberServiceStep04>();
    _legacyCode = new LegacyCodeStep07(_hash , _member);
}

[Test]
public void When_PassWord_Valid()
{
    _hash.GetDbMd5Hash(Arg.Any<string>()).Returns("Md5Password");
    _member.GetMd5Hash(Arg.Any<string>()).Returns("Md5Password");

    //  此時的輸入值其實不重要，因為要驗證的邏輯是，當密碼與DB密碼的MD5是一致的時候。
    var actual = _legacyCode.IsValid("Syuan", "Password");
    Assert.IsTrue(actual);
}
}